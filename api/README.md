# API back-end
An API that uses Postgraphile and Express to provide data to the front-end for visualisation.

## Getting set up
The following steps are required to get up and running:

1. Ensure that you are using the correct version of Node (as stated in `package.json->engines`), which can be verified using nvm:
```zsh
brew install nvm
nvm use 12
```
2. Set up your local environment in a `.env` file in the top level `/app` directory. The following variables are required:
- `DB_APP_SCHEMA`: schema containing data marts to be used for visualisations
- `DB_URL`: connection string for the database in JDBC format: `postgres://<user>:<password>@<host>:<port>/<database>`
- `DB_POSTGRAHILE_PORT`: port on which to expose the Postgraphile API

3. Install and run `yarn`, which is used for package management over `npm` due to its superior management of dependency resolutions (there are clashing dependencies between Postgraphile and graphql).
```zsh
brew install yarn
cd api
yarn install
```

4. Run the app:
```zsh
yarn run start
```


### Testing
Testing is configured using Jest, which will run all tests matching a `.*.spec.js` file name. Tests should be stored alongside the code they are testing in a `/test` folder, and named with `.unit.spec.js` or `.int.spec.js` extensions respectively for unit and integration tests.

To run the test suite, simply run:
```zsh
yarn run test
```