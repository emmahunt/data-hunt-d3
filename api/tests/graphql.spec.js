const debug = require('debug')('test:static');

// load config
const dotenv = require('dotenv');
dotenv.config();

// load GraphQL helpers
const { setup, teardown, runGraphQLQuery } = require("./helper/postgraphile");

describe('static', function() {
  
  afterAll(async () => {
    await teardown();
  });

  beforeAll(async () => {
    jest.setTimeout(30000);
    await setup();
  });

  it('should be able to run GraphQL query', async() => runGraphQLQuery(
      // GraphQL query goes here:
      `{ nodeId }`,
  
      // GraphQL variables go here:
      {},

      // requestion options
      {},
  
      // This function runs all your test assertions:
      async (json, { pgClient }) => {
        expect(json.errors).toBeFalsy();
        expect(json.data.nodeId).toBeTruthy();
      }
    )
  );

  it('should be able to get data from a schema', async() => runGraphQLQuery(
      // GraphQL query goes here:
      `{ locationByRoundedCoordinates { totalCount } }`,
  
      // GraphQL variables go here:
      {},

      // requestion options
      {},
  
      // This function runs all your test assertions:
      async (json, { pgClient }) => {
        expect(json.errors).toBeFalsy();
        expect(json.data.locationByRoundedCoordinates.totalCount).toBeGreaterThan(0);
      }
    )
  );

});
