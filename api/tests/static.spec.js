const debug = require('debug')('test:static');

// Use supertest.agent to store current user in req.user for passport requests
const supertest = require("supertest");

// load config
const dotenv = require('dotenv');
dotenv.config();

// load API
const { app } = require('../app.js');
const api = supertest.agent(app); 

describe('static', function() {

  afterAll(async () => {
    await api.close();
  });

  beforeAll(async () => {
    jest.setTimeout(30000);
    await api.get('/');
  });

  it('should get a response from GraphQL API', async () => {
    const res = await api
      .post('/graphql')
      .set('Content-Type', 'application/json')
      .send(
        JSON.stringify({
          query: `
            {
              query {
                nodeId
              }
            }`,
          variables: {},
        }))
        .expect('content-type', 'application/json; charset=utf-8');
    
    expect(res.statusCode).toBe(200);
    expect(res.body).toHaveProperty('data');
  });

});
