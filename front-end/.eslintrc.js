module.exports = {
  extends: ['eslint-config-airbnb-base', 'eslint-config-prettier'].map(require.resolve),
  parser: 'babel-eslint',
  env: {
    browser: true,
    mocha: true,
  },
  // baseline set of rules
  rules: {
    'consistent-return': 'warn',
    'no-unused-vars': 'warn',
    'default-case': 'warn',
    'no-shadow': 'warn',
    'no-array-constructor': 'warn',
    'no-param-reassign': 'warn',
    'vars-on-top': 'warn',
    'no-var': 'warn',
    'no-undef': 'warn',
    'no-multi-assign': 'warn',
    'prefer-destructuring': 'warn',
    'no-useless-escape': 'warn',
    'prefer-rest-params': 'warn',
    'import/no-duplicates': 'warn',
    'global-require': 'warn',
    'no-plusplus': 'warn',
    'import/order': 'warn',
    'no-use-before-define': 'warn',
    'no-bitwise': 'warn',
    'block-scoped-var': 'warn',
    'valid-typeof': 'warn',
    'no-nested-ternary': 'warn',
    'no-restricted-globals': 'warn',
    'no-empty-function': 'warn',
    'getter-return': 'warn',
    'no-cond-assign': 'warn',
    'import/no-unresolved': 'warn',

    // this overrides some airbnb defaults from eslint-config-airbnb-base
    'class-methods-use-this': 0,
    'no-console': 0,
    'import/no-extraneous-dependencies': 0,
    'import/extensions': 'off',
    'no-underscore-dangle': 'off',
    'import/prefer-default-export': 'off',
    'newline-per-chained-call': ['error', { ignoreChainWithDepth: 2 }],
  },
  settings: {
    'import/resolver': {
      node: {
        moduleDirectory: ['node_modules'],
      },
    },
  },
  plugins: ['html'],
  globals: {
    PACKAGE_VERSION: 'true',
  },
};
