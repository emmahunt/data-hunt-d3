import React from 'react';
import './App.css';

import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import DescriptionIcon from '@material-ui/icons/Description';
import InfoIcon from '@material-ui/icons/Info';
import MapSharpIcon from '@material-ui/icons/MapSharp';
import CodeSharpIcon from '@material-ui/icons/CodeSharp';
import { ThemeProvider } from '@material-ui/core/styles';
import Map from './components/Map/Map.js';

// styles
import useStyles from './styles/app.styles.js';
import theme from './styles/theme.js';

function App() {
  const classes = useStyles();

  return (
    <ThemeProvider theme={theme}>
      <div className="App">
        <AppBar position="sticky" className={classes.headerBar}>
          <Typography variant="h5">This is the Data Hunt project!</Typography>
        </AppBar>
        <Drawer
          variant="permanent"
          classes={{
            paper: classes.drawerPaper,
          }}
        >
          <List>
            {[
              ['About', <InfoIcon />],
              ['Map', <MapSharpIcon />],
              ['Source', <CodeSharpIcon />],
            ].map((item, index) => (
              <ListItem button key={index}>
                <ListItemIcon>{item[1]}</ListItemIcon>
                <ListItemText primary={item[0]} />
              </ListItem>
            ))}
          </List>
          <Divider />
          <List>
            {['Documentation'].map((text, index) => (
              <ListItem button key={text}>
                <ListItemIcon>
                  <DescriptionIcon />
                </ListItemIcon>
                <ListItemText primary={text} />
              </ListItem>
            ))}
          </List>
        </Drawer>
        <Container className={classes.appContents}>
          <Map />
        </Container>
      </div>
    </ThemeProvider>
  );
}

export default App;
