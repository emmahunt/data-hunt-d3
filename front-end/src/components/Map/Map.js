import React, { useState } from 'react';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { useQuery } from '@apollo/react-hooks';
import gql from 'graphql-tag';
import Button from '@material-ui/core/Button';

// Import components
import AusMap from './visualisations/AusMap.js';
import MapBox from './visualisations/MapBox.js';

const queryString = gql`
  {
    locationByRoundedCoordinates {
      nodes {
        roundedLatitude
        roundedLongitude
        numberOfPoints
      }
    }
  }
`;

const Map = () => {
  // Use the Apollo 'useQuery' hook to get data and error / loading status on component load
  const { loading, error, data } = useQuery(queryString);
  const height = 1000;
  const width = 500;
  const latitude = -23;
  const longitude = 133;
  const zoom = 3;

  const [mapLoaded, setMapLoaded] = useState(0);
  const [map, setMap] = useState();

  function locJump(long, lat, zoom) {
    map.jumpTo({
      center: [long, lat],
      zoom,
    });
  }

  const loadMap = () => (
    <MapBox
      lat={latitude}
      long={longitude}
      zoom={zoom}
      size={[height, width]}
      setMap={setMap}
      setMapLoaded={setMapLoaded}
    />
  );

  // Function to enable loading message to display if map has not been loaded yet. This prevents an error in the children components from attempting to attach an SVG to a non-existent map
  const loadAusMap = () => {
    if (mapLoaded > 0) {
      return (
        <AusMap
          size={[height, width]}
          data={data.locationByRoundedCoordinates.nodes}
          map={map}
        />
      );
    }
    return 'Loading map...';
  };

  // Return a loading message until data is loaded, unless there is an error
  if (loading) return 'Loading Data...';
  if (error) return `Error! ${error.message}`;

  return (
    <Box>
      <Typography variant="h1">Map of Locations Visited</Typography>
      <Button variant="outlined" onClick={() => locJump(116, -31.95, 10)}>
        Jump to Perth
      </Button>
      <Button variant="outlined" onClick={() => locJump(-0.1, 51.5, 10)}>
        Jump to London
      </Button>
      <Button variant="outlined" onClick={() => locJump(longitude, latitude, 3)}>
        Reset
      </Button>
      {loadMap()}
      {loadAusMap()}
    </Box>
  );
};

export default Map;
