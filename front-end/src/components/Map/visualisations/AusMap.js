import React from 'react';
import { select, scaleSequential, interpolateViridis } from 'd3';
import mapboxgl from 'mapbox-gl';

const colourScale = scaleSequential().domain([1, 10]).interpolator(interpolateViridis);

// When a data point is hovered over, change the styling of the point and add a text label
function mouseOver(d, currentObject, colourScale, svg, projection) {
  select(currentObject)
    .attr('r', 12)
    .style('fill', d => colourScale(d.numberOfPoints))
    .attr('fill-opacity', 0.4)
    .attr('stroke', d => colourScale(d.numberOfPoints))
    .attr('stroke-width', 3);

  appendTextLabel(d, svg, projection);
}

// When a data point is no longer hovered over, revert the styling of the point and remove the text label
function mouseOut(d, currentObject, colourScale) {
  select(currentObject)
    .attr('r', 4)
    .style('fill', d => colourScale(d.numberOfPoints))
    .attr('stroke-width', 0)
    .attr('fill-opacity', 1);

  // Select text by id and then remove
  select('#text-label').remove();
}

// Define text label that will be created on hover-over
function appendTextLabel(d, svg, projection) {
  svg
    .append('text')
    // Create an id for text so we can select it later for removing on mouseout
    .attr('id', 'text-label')
    .attr('x', d => {
      const { x } = projection;
      return x - 15;
    })
    .attr('y', d => {
      const { y } = projection;
      return y - 15;
    })
    .text(() => [`Number of times visited : ${d.numberOfPoints}`]);
}

function project(map, d) {
  return map.project(getLL(d));
}

function getLL(d) {
  return new mapboxgl.LngLat(d.roundedLongitude, d.roundedLatitude);
}

function render(map, dots) {
  dots
    .attr('cx', d => {
      const { x } = project(map, d);
      return x;
    })
    .attr('cy', d => {
      const { y } = project(map, d);
      return y;
    });
}

function AusMap(props) {
  const locData = props.data;
  const { map } = props;
  const container = map.getCanvasContainer();

  // Initialise svg (with styling) which will contain the location data points. This is appended to the map object
  const svg = select(container)
    .append('svg')
    .attr('width', props.size[0])
    .attr('height', props.size[1])
    .style('position', 'relative');

  const dots = svg
    .selectAll('circle.dot')
    .data(locData)
    .enter()
    .append('circle')
    .classed('dot', true)
    .attr('r', 1)
    .style('fill', d => colourScale(d.numberOfPoints))
    .attr('r', 6)
    .on('mouseover', function (d) {
      return mouseOver(d, this, colourScale, svg, project(map, d));
    })
    // Remove the label and reset node formatting when mouse is no longer hovering over node
    .on('mouseout', function (d) {
      return mouseOut(d, this, colourScale);
    });

  // re-render our visualization whenever the view changes
  map.on('viewreset', () => {
    render(map, dots);
  });
  map.on('move', () => {
    render(map, dots);
  });

  // render our initial visualization
  render(map, dots);

  return <React.Fragment></React.Fragment>;
}
export default AusMap;
