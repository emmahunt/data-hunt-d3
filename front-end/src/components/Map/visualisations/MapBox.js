import React, { useEffect, useRef } from 'react';

import mapboxgl from 'mapbox-gl';
import 'mapbox-gl/dist/mapbox-gl.css';

function MapBox(props) {
  const styles = {
    width: props.size[0],
    height: props.size[1],
    // position: "absolute"
  };

  const mapContainer = useRef(null);
  mapboxgl.accessToken = process.env.REACT_APP_MAPBOX_TOKEN;

  useEffect(() => {
    const map = new mapboxgl.Map({
      container: mapContainer.current,
      style: process.env.REACT_APP_MAPBOX_STYLE_KEY, // stylesheet location
      center: [props.long, props.lat],
      zoom: props.zoom,
    });
    map.addControl(new mapboxgl.NavigationControl());

    props.setMapLoaded(1);
    props.setMap(map);

    // if (!map) initializeMap({ props.setMap, mapContainer });
  }, []);

  return <div ref={mapContainer} style={styles} />;
}

export default MapBox;
