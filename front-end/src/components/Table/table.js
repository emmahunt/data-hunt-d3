import { ApolloTableQL } from 'react-tableql';
import React from 'react';

const GET_TOTAL_LOCATION_COUNT = `
{
  locationByRoundedCoordinates(condition: {
    roundedLatitude: "-32.001"}) {
    nodes {
      roundedLatitude
      roundedLongitude
      numberOfPoints
    }
  }
}
`;

function Table() {
  return (
    <div className="Table">
      <ApolloTableQL query={GET_TOTAL_LOCATION_COUNT} />
    </div>
  );
}

export default Table;
