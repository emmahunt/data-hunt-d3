import { makeStyles } from '@material-ui/core/styles';

const drawerWidth = 20;
const headerBarHeight = 60;

const useStyles = makeStyles(theme => ({
  headerBar: {
    minHeight: headerBarHeight,
    justifyContent: 'center',
    elevation: '0',
    position: 'fixed',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: `${drawerWidth}%`,
    top: headerBarHeight,
  },
  appContents: {
    width: `${100 - drawerWidth}%`,
    float: 'right',
    top: headerBarHeight,
    position: 'relative',
  },
}));

export default useStyles;
