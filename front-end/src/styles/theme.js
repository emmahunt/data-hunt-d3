import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
  shadows: ['none'],
});

export default theme;
